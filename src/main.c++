#include "any.h++"
#include "star.h++"
#include "symbol.h++"

#include <iostream>

/**
 * @brief stringToIterables convert wilcard to iterable objects
 * @param str is the wildcard expression
 * @return the pointer to first node, nullptr if str is empty
 */
Iterable *stringToIterables(const std::string &str) {
  Iterable *first = nullptr;
  Iterable *current = nullptr;

  for (auto &ch : str) {
    Iterable *toAdd = nullptr;

    if (ch == '*') {
      toAdd = new Star;
    } else if (ch == '?') {
      toAdd = new Any;
    } else {
      toAdd = new Symbol{ch};
    }

    if (current == nullptr) {
      first = current = toAdd;
    } else {
      current->setNext(toAdd);
      current = toAdd;
    }
  }

  return first;
}

/**
 * @brief check checks if the text matches the wildcard
 * @param wildcard us the wildcard expession
 * @param text is the text to match
 * @return true if text matches wildcard, otherwise false
 */
bool check(const std::string &wildcard, const std::string &text) {
  Iterable *iterable = stringToIterables(wildcard);
  bool result = iterable->check(text, 0);

  delete iterable;
  return result;
}

/**
 * @brief main is the main function of programm
 * @return always 0, that means "All is ok!"
 */
int main() {
  std::cout << "wildcard string  result" << std::endl;
  std::cout << "*.h++    any.h++ " << check("*.h++", "any.h++") << std::endl;
  std::cout << "*.h++    any.c++ " << check("*.h++", "any.c++") << std::endl;
  std::cout << "*?.x     x.x     " << check("*?.x", "x.x") << std::endl;
  std::cout << "?*.x     x.x     " << check("?*.x", "x.x") << std::endl;
  std::cout << "**.x     x.x     " << check("**.x", "x.x") << std::endl;
  std::cout << "x.x?     x.x     " << check("x.x?", "x.x") << std::endl;
  std::cout << "x.x?     x.xy    " << check("x.x?", "x.xy") << std::endl;
  std::cout << "x.x?     x.xyz   " << check("x.x?", "x.xyz") << std::endl;
  std::cout << "x.x?     x.      " << check("x.x?", "x.xyz") << std::endl;

  return 0;
}
