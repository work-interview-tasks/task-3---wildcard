#pragma once

#include "iterable.h++"

class Star : public Iterable {
  int position = -1;

  // Iterable interface
protected:
  int next(const std::string &str, int current) override;
  void reset() override;
};
