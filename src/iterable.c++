#include "iterable.h++"

Iterable::~Iterable() { delete m_next; }

void Iterable::setNext(Iterable *next) { m_next = next; }

bool Iterable::check(const std::string &str, int current) {
  reset();

  int nextIndex = next(str, current);
  bool success = false;

  // Recursion ends here
  if (m_next == nullptr) {
    return nextIndex != -1 && size_t(nextIndex) == str.length();
  }

  while (nextIndex != -1 && !success) {
    success = m_next->check(str, nextIndex);
    nextIndex = next(str, current);
  }

  return success;
}
