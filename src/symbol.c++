#include "symbol.h++"

int Symbol::next(const std::string &str, int current) {
  if (triggered) {
    return -1;
  }

  triggered = true;
  return str[size_t(current)] == symbol ? current + 1 : -1;
}

void Symbol::reset() { triggered = false; }
