#include "any.h++"

int Any::next(const std::string &str, int current) {
  if (triggered) {
    return -1;
  }

  triggered = true;
  return size_t(current) == str.length() ? current : current + 1;
}

void Any::reset() { triggered = false; }
