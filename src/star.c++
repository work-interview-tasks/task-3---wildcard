#include "star.h++"

int Star::next(const std::string &str, int current) {
  if (position == -1) {
    position = current;
    return position;
  }

  return size_t(position) != str.length() ? ++position : -1;
}

void Star::reset() { position = -1; }
