#pragma once

#include <string>

/**
 * @brief The Iterable class checks some variants of string continuation
 */
class Iterable {
public:
  /**
   * An interface must have a virtual descructor
   */
  virtual ~Iterable();

protected:
  /**
   * @brief next gets the index of next possible token
   * @return the index of next possible token, on failed returns -1
   */
  virtual int next(const std::string &str, int current) = 0;

  /**
   * @brief reset reset the state of iterable
   */
  virtual void reset() = 0;

public:
  /**
   * @brief setNext set the next token in grammatics
   * @param next is the next, also iterable, token
   */
  void setNext(Iterable *next);

  /**
   * @brief check checks if string match this rule
   * @param str is the string to match
   * @param current is the cursor position, begins with 0
   * @return true if string matches the wildcard, otherwise false
   */
  bool check(const std::string &str, int current);

private:
  Iterable *m_next = nullptr;
};
