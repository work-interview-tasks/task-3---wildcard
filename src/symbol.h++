#pragma once

#include "iterable.h++"

class Symbol : public Iterable {
  char symbol = 0;
  bool triggered = false;

public:
  Symbol(char symbol) : symbol(symbol) {}

  // Iterable interface
protected:
  int next(const std::string &str, int current) override;
  void reset() override;
};
