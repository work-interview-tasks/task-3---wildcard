#pragma once

#include "iterable.h++"

class Any : public Iterable {
  bool triggered = false;

  // Iterable interface
protected:
  int next(const std::string &str, int current) override;
  void reset() override;
};
